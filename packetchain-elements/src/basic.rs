use std::sync::atomic::{Ordering};
use std::str::from_utf8;
use packetchain_core;

pub struct MuffinMan<'a> {
    pub output: &'a packetchain_core::Ring<'a>,
}

impl<'a> MuffinMan<'a> {

    pub fn run(&self) {

        println!("sending muffin query");

        let mut index = self.output.reserve(6);
        for s in &["do", "you", "know", "the", "muffin", "man"] {

            self.output.get(index).write(s.as_bytes());
            index += 1;

        }

    }

}

pub struct Passthru<'a> {
    pub input: &'a packetchain_core::Ring<'a>,
    pub output: &'a packetchain_core::Ring<'a>,
}

impl<'a> Passthru<'a> {

    pub fn run(&self) {

        let ibegin = self.input.tail.load(Ordering::Relaxed);
        let iend = self.input.head.load(Ordering::Relaxed);
        let len = self.input.len();

        let mut obegin = self.output.reserve(len);

        for i in ibegin..iend {

            self.output.set(obegin, self.input.get(i));
            obegin += 1;

        }

        self.input.free(len);

    }

}

pub struct Printer<'a> {
    pub input: &'a packetchain_core::Ring<'a>,
}

impl<'a> Printer<'a> {

    pub fn run(&self) {

        let ibegin = self.input.tail.load(Ordering::Relaxed);
        let iend = self.input.head.load(Ordering::Relaxed);
        let len = self.input.len();

        for i in ibegin..iend {

            let x = &*self.input.get(i).data.read().expect("llama");
            println!("{}", from_utf8(x).unwrap())

        }

        self.input.free(len);

    }

}

