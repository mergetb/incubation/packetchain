use std::io;
use std::io::Write;
use std::sync::RwLock;
use std::sync::atomic::{AtomicUsize, Ordering};

const FRAMESIZE: usize = 9216;

pub struct Frame {
    pub data: RwLock<[u8; FRAMESIZE]>,
    pub len: AtomicUsize,
}

impl Frame {

    pub fn write(&self, buf: &[u8]) {

        for (i, v) in buf.iter().enumerate() {
            self.data.write().expect("llama")[i] = *v;
        }
        self.len.store(buf.len(), Ordering::Relaxed);
    }

}

impl Clone for Frame {

    fn clone(&self) -> Self {

        Frame{
            data: RwLock::new(self.data.read().expect("llama").clone()),
            len: AtomicUsize::new(self.len.load(Ordering::Relaxed)),
        }


    }
}

#[derive(Clone)]
pub struct FrameDescriptor {
    pub addr: usize,
}

pub struct Ring<'a> {
    pub frames: Vec<FrameDescriptor>,
    pub arena: &'a Arena,
    pub head: AtomicUsize,
    pub tail: AtomicUsize,
    pub size: usize,
}

impl<'a> Ring<'a> {

    pub fn new(arena: &Arena, mut addr: usize, size: usize) -> Ring {

        let mut r = Ring{
            frames: vec![FrameDescriptor{addr:0}; size],
            arena: arena,
            head: AtomicUsize::new(0),
            tail: AtomicUsize::new(0),
            size: size,
        };

        for d in &mut r.frames {
            d.addr = addr;
            addr = addr+1;
        }

        r

    }

    pub fn reserve(&self, count: usize) -> usize {

        let current = self.head.load(Ordering::Relaxed);
        self.head.store((current + count) % self.size, Ordering::Relaxed);

        return current;

    }

    pub fn free(&self, count: usize) -> usize {

        let current = self.tail.load(Ordering::Relaxed);
        self.tail.store((current + count) % self.size, Ordering::Relaxed);

        return current;

    }

    pub fn len(&self) -> usize {

        (self.head.load(Ordering::Relaxed) - 
            self.tail.load(Ordering::Relaxed)) % self.size

    }

    pub fn get(&self, index: usize) -> &Frame {

        return &self.arena.data[self.frames[index].addr]

    }

    pub fn set(&self, index: usize, frame: &Frame) {

        self.arena.data[self.frames[index].addr].write(
            &*frame.data.read().expect("llama")
        );
    }


}

pub struct Arena {
    pub data: Vec<Frame>
}

impl Arena {

    pub fn new(size: usize) -> Arena {

        print!("creating memory areana ... ");
        io::stdout().flush().unwrap();

        let arena = Arena{
            data: vec![Frame{
                data: RwLock::new([0; FRAMESIZE]),
                len: AtomicUsize::new(0),
            }; size]
        };

        println!("done");

        return arena;

    }
}
