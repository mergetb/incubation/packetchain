use clap::{App, Arg};
use packetchain_core;
use packetchain_elements;
use crossbeam;

fn main() {

    let matches = App::new("packetchain")
        .version("0.1.0")
        .about("A lightweight network emulator")
        .arg(Arg::with_name("arena")
                 .short("a")
                 .long("arena")
                 .takes_value(true)
                 .help("arena size in frames"))
        .arg(Arg::with_name("ring")
                 .short("r")
                 .long("ring")
                 .takes_value(true)
                 .help("ring size in frame descriptors"))
        .get_matches();

    let mut ring_size = 4096;
    let mut arena_size = ring_size*10;

    let arena = matches.value_of("arena");
    match arena {
        None => {}
        Some(s) => {
            match s.parse::<usize>() {
                Ok(n) => arena_size =  n,
                Err(_) => println!("arena size must be an unsigned integer"),
            }
        }
    }

    let ring = matches.value_of("ring");
    match ring {
        None => {}
        Some(s) => {
            match s.parse::<usize>() {
                Ok(n) => ring_size =  n,
                Err(_) => println!("ring size must be an unsigned integer"),
            }
        }
    }

    let arena = packetchain_core::Arena::new(arena_size);

    let r0 = packetchain_core::Ring::new(&arena, 0, ring_size);
    let r1 = packetchain_core::Ring::new(&arena, ring_size, ring_size);

    let mm = packetchain_elements::MuffinMan{
        output: &r0,
    };

    let pt = packetchain_elements::Passthru{
        input: &r0,
        output: &r1,
    };

    let pr = packetchain_elements::Printer{
        input: &r1,
    };

    crossbeam::scope(|s| {
        s.spawn(|_| {

            mm.run();
            pt.run();
            pr.run();

        });
    }).unwrap();

}
